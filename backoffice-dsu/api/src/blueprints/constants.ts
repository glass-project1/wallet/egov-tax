import {getBlueprintInjectables, setBlueprintInjectables} from "@glass-project1/dsu-blueprint";
import {toolkitInjectables} from "@glass-project1/glass-toolkit";
import {EGovTaxDAppGr} from "./EGovTaxDAppGr";
import {EGovTaxDAppPt} from "./EGovTaxDAppPt";
import {EGovTaxDAppTk} from "./EGovTaxDAppTk";
import {EGovTaxGr} from "./EGovTaxGr";
import {EGovTaxPt} from "./EGovTaxPt";
import {EGovTaxTk} from "./EGovTaxTk";

import type {EGovTaxInjectables} from "./types";

export const eGovTaxInjectables: EGovTaxInjectables = {
    ...toolkitInjectables,
    blueprints: {
        ...toolkitInjectables.blueprints,

        EGovTaxGr,
        EGovTaxPt,
        EGovTaxTk,

        EGovTaxDAppGr,
        EGovTaxDAppPt,
        EGovTaxDAppTk
    }
}

export function getEGovTaxInjectables(): EGovTaxInjectables {
    setBlueprintInjectables(eGovTaxInjectables, true);
    return getBlueprintInjectables() as EGovTaxInjectables;
}