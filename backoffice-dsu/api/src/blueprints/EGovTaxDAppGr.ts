import type {Constructor} from "@glass-project1/dsu-blueprint";
import type {IEGovTaxDApp, IGlassDID} from "@glass-project1/glass-toolkit";
import type {DSUDatabase, DSUEnclave, DSUSecurityContext, EnvironmentDefinition} from "@glass-project1/opendsu-types";
import type {EGovTaxInjectables} from "./types";

/**
 * {@link DSUBlueprint} decorated Builtin Class representing the {@link EGovTaxDAppGr}
 *
 * Note that it contains an {@link EGovTax} under 'tax'
 *
 * @class EGovTaxDAppGr
 * @extends DBModel
 */
export function EGovTaxDAppGr(injectables: EGovTaxInjectables): Constructor<IEGovTaxDApp>
export function EGovTaxDAppGr(injectables: EGovTaxInjectables, data: Record<string, any>): IEGovTaxDApp
export function EGovTaxDAppGr(injectables: EGovTaxInjectables, data?: Record<string, any>): Constructor<IEGovTaxDApp> | IEGovTaxDApp {

    const EGovTaxClass = injectables.blueprints.EGovTaxGr(injectables);

    @injectables.DSUBlueprint(undefined, injectables.KeySSIType.SEED)
    class EGovTaxDAppGr extends injectables.DBModel {

        @injectables.dsuMixinBlueprint(injectables.blueprints.EGovTaxGr, injectables, true, true)
        tax?: any = undefined;

        @injectables.signedDID(undefined)
        did?: IGlassDID = undefined;

        @injectables.enclave()
        enclave?: DSUEnclave = undefined;

        @injectables.environment()
        environment?: EnvironmentDefinition = undefined;

        @injectables.walletDB()
        db?: DSUDatabase = undefined;

        @injectables.securityContext()
        sc?: DSUSecurityContext = undefined;

        constructor(eGovIDdApp?: EGovTaxDAppGr | {}) {
            super();
            injectables.constructFromBlueprint<EGovTaxDAppGr>(this, eGovIDdApp);
            this.tax = new EGovTaxClass(this.tax);
            this.did = new injectables.GlassDID(this.did);
        }
    }

    if (data)
        return new EGovTaxDAppGr(data);
    return EGovTaxDAppGr;
}