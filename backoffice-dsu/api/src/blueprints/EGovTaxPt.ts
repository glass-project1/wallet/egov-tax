import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovTax, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EU Tax record
 *
 * @class EGovTaxPt
 * @extends EvidenceBlueprint
 */
export function EGovTaxPt(injectables: ToolkitInjectables): Constructor<IEGovTax>
export function EGovTaxPt(injectables: ToolkitInjectables, data: DataProperties<IEGovTax>): IEGovTax
export function EGovTaxPt(injectables: ToolkitInjectables, data?: DataProperties<IEGovTax>): Constructor<IEGovTax> | IEGovTax {

    @injectables.RenderableDSUBlueprint("glass-tax-income-pt", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-tax-income-pt", "../components/components", undefined, false)
    class EGovTaxPt extends injectables.EvidenceBlueprint {

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.uiprop("declaration-year")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        incomeDeclarationFiscalYear?: string = undefined;

        @injectables.uiprop("declared-income")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        declaredIncome?: string = undefined;

        @injectables.uiprop("income-tax")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        incomeTax?: string = undefined;


        constructor(EGovTaxPt?: EGovTaxPt | {}) {
            super(EGovTaxPt);
            injectables.constructFromBlueprint<EGovTaxPt>(this, EGovTaxPt);
        }
    }

    if (data)
        return new EGovTaxPt(data);
    return EGovTaxPt;
}