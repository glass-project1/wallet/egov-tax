import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovTax, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EU Tax record
 *
 * @class EGovTaxTk
 * @extends EvidenceBlueprint
 */
export function EGovTaxTk(injectables: ToolkitInjectables): Constructor<IEGovTax>
export function EGovTaxTk(injectables: ToolkitInjectables, data: DataProperties<IEGovTax>): IEGovTax
export function EGovTaxTk(injectables: ToolkitInjectables, data?: DataProperties<IEGovTax>): Constructor<IEGovTax> | IEGovTax {

    @injectables.RenderableDSUBlueprint("glass-tax-income-tk", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-tax-income-tk", "../components/components", undefined, false)
    class EGovTaxTk extends injectables.EvidenceBlueprint {

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.uiprop("declaration-year")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        incomeDeclarationFiscalYear?: string = undefined;

        @injectables.uiprop("declared-income")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        declaredIncome?: string = undefined;

        @injectables.uiprop("income-tax")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        incomeTax?: string = undefined;

        constructor(EGovTaxTk?: EGovTaxTk | {}) {
            super(EGovTaxTk);
            injectables.constructFromBlueprint<EGovTaxTk>(this, EGovTaxTk);
        }
    }

    if (data)
        return new EGovTaxTk(data);
    return EGovTaxTk;
}