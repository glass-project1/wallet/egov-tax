import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovTax, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EU Tax record
 *
 * @class EGovTaxGr
 * @extends EvidenceBlueprint
 */
export function EGovTaxGr(injectables: ToolkitInjectables): Constructor<IEGovTax>
export function EGovTaxGr(injectables: ToolkitInjectables, data: DataProperties<IEGovTax>): IEGovTax
export function EGovTaxGr(injectables: ToolkitInjectables, data?: DataProperties<IEGovTax>): Constructor<IEGovTax> | IEGovTax {

    @injectables.RenderableDSUBlueprint("glass-tax-income-gr", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-tax-income-gr", "../components/components", undefined, false)
    class EGovTaxGr extends injectables.EvidenceBlueprint {

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.uiprop("declaration-year")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        incomeDeclarationFiscalYear?: string = undefined;

        @injectables.uiprop("declared-income")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        declaredIncome?: string = undefined;

        @injectables.uiprop("income-tax")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        incomeTax?: string = undefined;


        constructor(EGovTaxGr?: EGovTaxGr | {}) {
            super(EGovTaxGr);
            injectables.constructFromBlueprint<EGovTaxGr>(this, EGovTaxGr);
        }
    }

    if (data)
        return new EGovTaxGr(data);
    return EGovTaxGr;
}