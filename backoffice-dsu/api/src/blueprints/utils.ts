import {Constructor} from "@glass-project1/dsu-blueprint";
import {eGovTaxInjectables} from "./constants";
import type {EGovTaxInjectables} from "./types";

type EGovTaxBlueprintFunction<T> = (injectables: EGovTaxInjectables) => Constructor<T>;

export function getBlueprint<T>(blueprintName: string, countryCode: string): EGovTaxBlueprintFunction<T> | undefined {
    countryCode = countryCode.toLowerCase();
    countryCode = countryCode.charAt(0).toUpperCase() + countryCode.slice(1);
    return eGovTaxInjectables.blueprints[blueprintName + countryCode];
}