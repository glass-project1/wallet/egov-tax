import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';



@Entity("egovtax")
export class EGovTaxEntity extends BaseEntity {

    @ApiProperty({ description: "CCV_00022_IDnumber Mandatory Citizen Identification Number" })
    @PrimaryColumn()
    id: string;

    @ApiProperty({ description: "CCV_00002_name Given names" })
    @Column({ name: "givenname" })
    givenName: string;

    @ApiProperty({ description: "CCV_00003_Surname Surnames" })
    @Column()
    surname: string;

    @ApiProperty({ description: "CCV_00047_incomeDeclarationFiscalYear" })
    @Column({ name: "incomedeclarationfiscalyear" })
    incomeDeclarationFiscalYear: string;
    
    @ApiProperty({ description: "CCV_00048_declaredIncome" })
    @Column({ name: "declaredincome" })
    declaredIncome: string;
    
    @ApiProperty({ description: "CCV_00049_incomeTax" })
    @Column({ name: "incometax" })
    incomeTax: string;

    @ApiProperty({ description: "A string with the keySSI. Non-null if the wallet has already been created.", required: false })
    @Column({ name: "walletkeyssi" })
    walletKeySSI: string;

    @ApiProperty({ description: "ISO-3166-1 alpha-2 two letter country code to which this record belongs. Normally, all the records in the same agency belong to the same country, but this column exists for developers to be allowed to mix records from distinct countries.", required: false })
    @Column({ name: "country" })
    country: string;
}
