import {HttpService} from '@nestjs/axios';
import {Injectable, NotFoundException} from '@nestjs/common';
import {Connection, EntityManager} from 'typeorm';
import {getEGovTaxInjectables} from "../blueprints/constants";
import {getBlueprint} from "../blueprints/utils";
import {EGovTaxEntity} from './egovtax.entity';
import {
    EGovServiceWallet, GlassDID,
    GlassWalletManager, IEGovCriminalRecordDApp,
    MarketKeys,
    registerGlassDIDMethod,
    toolkitInjectables
} from '@glass-project1/glass-toolkit';

import {EGovTaxDApp} from '@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovTaxDApp';
import {debug, Err, errorCallback, info, LoggedError} from '@glass-project1/logging';
import {DSU, DSUDid, getOpenDSU, KeySSI, OpenDSU} from "@glass-project1/opendsu-types";
import {OpenDSURepository} from '@glass-project1/dsu-blueprint';
import {DBModel, inject} from '@glass-project1/db-decorators';
import {EGovTaxRepository} from './egovtax.repository';
import {AppResourceRepository} from './appresource.repository';
import {generateWalletEnvironment, getEnvironmentFromProcess} from "@glass-project1/glass-toolkit/lib/providers";
import {BackendFileServiceImp} from "@glass-project1/glass-toolkit/lib/providers/BackendFileService";
import {ServiceWalletBooter} from "../ServiceWalletBooter";

/*
   Imports from outside the api folder will force tsconfig to re-create the dist folder structure.
   These where used for the model classes, but once the model moved into glass-toolkit,
   it is no longer needed!
   privatesky is imported using require.
*/
// import { Dummy } from "../../../src/model"; // this works, but forces dist/api/src
// import { EGovId } from "./model";
/* the above line does not work, even if adding to tsconfig.json

    "compilerOptions": {
      "rootDirs": ["./src", "../src"] // or "../../src"
    }
*/
//import {safeParseKeySSI} from "@glass-project1/dsu-blueprint/lib";
//import opendsu = require('../../../privatesky/psknode/bundles/openDSU'); // if here, will break https://gitlab.com/glass-project1/wallet/egov-id/-/issues/1#note_27486


@Injectable()
export class EGovTaxService {

    private arcRepository: AppResourceRepository;
    private eGovTaxRepository: EGovTaxRepository;

    @inject("GlassWalletManager")
    walletManager!: GlassWalletManager;

    @inject("ServiceWalletBooter")
    walletBooter!: ServiceWalletBooter;

    constructor(
        private connection: Connection,
        private readonly httpService: HttpService
    ) {
        this.arcRepository = this.connection.getCustomRepository(AppResourceRepository);
        this.eGovTaxRepository = this.connection.getCustomRepository(EGovTaxRepository);
    }

    /**
     * Create (INSERT) a new EGovTax from DTO JSON data in a single transaction.
     * @param eGovTaxDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async create(eGovTaxDto: any) {
        const self: EGovTaxService = this;
        await this.connection.transaction(async tem => {
            await self.createT(tem, eGovTaxDto);
        });
    }

    /**
     * Create (INSERT) a new ClinicalTrial from DTO JSON data, given a transactional entity manager.
     * @param tem Transactional EntityManager
     * @param egovtaxDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async createT(tem: EntityManager, egovtaxDto: any) {
        await tem.save(EGovTaxEntity, egovtaxDto);
    }

    /**
     * Update (SQL UPDATE) a EGovId from DTO JSON data in a single transaction.
     * @param eGovTaxDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async update(eGovTaxDto: any) {
        const self = this;
        await this.connection.transaction(async tem => {
            await self.updateT(tem, eGovTaxDto);
        });
    }

    /**
     * Update (SQL UPDATE) a EGovTax from DTO JSON data in a single transaction.
     * @param tem Transactional EntityManager
     * @param eGovTaxDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async updateT(tem: EntityManager, eGovTaxDto: any) {
        await tem.save(EGovTaxEntity, eGovTaxDto); // autocommit is good enough ?
    }

    /**
     * Create a new DSU wallet for a given ID number.
     * If this ID already has a wallet, the same wallet is returned.
     * No more than one wallet can be created for the same ID.
     *
     * The algorithm is as follows:
     * step 1 - BEGIN TRANSACTION
     * step 2 - SELECT FOR UPDATE to lock the record being modified.
     * step 3 - If alredy has a wallet, return it and stop here.
     * step 4 - Create the DSU using an external DSU service with a callback
     * step 5 - UPDATE the record previously locked
     * step 6 - COMMIT
     *
     * Error handling must rollback and release the transaction.
     *
     * @param {string} eGovTaxId ID of the record to create the wallet.
     */
    async createWallet(eGovTaxId: string): Promise<EGovTaxEntity> {
        const self = this;
        const debugId = "egovtax createWallet "+eGovTaxId;
        let openDsuLockedFlag = false;
        let eGovTaxEntity: EGovTaxEntity = undefined;

        let resultResolve;
        let resultReject;
        const result = new Promise<EGovTaxEntity>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.startTransaction();
        result.catch(async (err) => {
            // from now on, if we reject the result, we need to rollback and close the DB query.
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            // throw new Error(err); // do not (re) throw, or nestjs will die. Rejection is enough.
        });
        try {
            eGovTaxEntity = await queryRunner.manager.findOne(EGovTaxEntity, eGovTaxId, { lock: { mode: "pessimistic_write" } });
            if (!eGovTaxEntity) {
                //await queryRunner.rollbackTransaction();
                //await queryRunner.release();
                throw new NotFoundException("No EGovTax.id=" + eGovTaxId);
            }

            if (eGovTaxEntity.walletKeySSI) {
                // keySSI already filled, means that wallet is already existing.
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                resultResolve(eGovTaxEntity);
                return result; // createWallet must return the promise
            }

            /*
            let keyssispace = getKeySSIApi();
            const aSeedSSI = keyssispace.createTemplateSeedSSI("egovid", `STWHS000001-WHS000001-${(new Date()).toISOString()}`, 'v0');
            console.log(aSeedSSI.getIdentifier(true));
            */


            const env = self.walletBooter.environment;

            console.log("EGovTaxEntity", eGovTaxEntity);

            const EGovTaxDApp = getBlueprint<IEGovCriminalRecordDApp>("EGovTaxDApp", self.walletBooter.countryCode);
            if (!EGovTaxDApp)
                return resultReject(new LoggedError(`Invalid EGovTaxDApp for ${self.walletBooter.countryCode} country code`, self));

            const EGovTaxDAppConstr = EGovTaxDApp(getEGovTaxInjectables());
            const eGovTaxDAppDSU = new EGovTaxDAppConstr({
                tax: {
                    ...eGovTaxEntity,
                    __metadata: {
                        designation: {
                            en: "Tax Record"
                        },
                        identifier: `egov.${self.walletBooter.countryCode}.mof.tax.${eGovTaxEntity.id}`
                    }
                },
                did: {
                    data : {
                        name: [eGovTaxEntity.givenName, eGovTaxEntity.surname].join(' '),
                        identifier: `individual.egov.${self.walletBooter.countryCode}.mof.tax.${eGovTaxEntity.id}`,
                        publishes: [
                            `individual.egov.${self.walletBooter.countryCode}.mof.tax.${eGovTaxEntity.id}.**.*`
                        ]
                    }
                },
                environment: generateWalletEnvironment("egovtax-dapp", env)
            });

            const keyCache : {domain: string, [indexer: string]: any} = {"domain": env.domain};
            keyCache[MarketKeys.DOMAIN] = env.domain; //sets the market domain flag

            //const repo = new OpenDSURepository(EGovTaxDApp, env.domain, agencyDID, undefined, keyCache);
            // eGovTaxDAppDSU.did = undefined; egov#4 uncomment to force error on callback
            //repo.create(eGovTaxDAppDSU, async (err, updatedModel, dsu, keySSI) => {
            self.walletManager.issueEvidence(keyCache, eGovTaxDAppDSU, undefined, EGovTaxDApp, async (err, updatedModel, dsu, keySSI) => {
                debug(`walletManager.issueEvidence eGovTaxDAppDSU returned ${err}, ${updatedModel}, ${keySSI ? keySSI.getIdentifier(true) : keySSI, eGovTaxDAppDSU}`);
                if (err) {
                    // throw err; egov#4 throw is not always caught by catch
                    //await queryRunner.rollbackTransaction();
                    //await queryRunner.release();
                    return resultReject(new LoggedError(err, self));
                }
                eGovTaxEntity.walletKeySSI = keySSI.getIdentifier(true);
                //await this.enrichKeySSI(eGovTaxEntity);
                await queryRunner.manager.save(EGovTaxEntity, eGovTaxEntity);
                await queryRunner.commitTransaction();
                await queryRunner.release();
                resultResolve(eGovTaxEntity);
                return; // callback has to return nothing
            });
        } catch (err) {
            //await queryRunner.rollbackTransaction();
            //await queryRunner.release();
            resultReject(err);
        }

        // Don't add the finally. It will be called before the repo.create(...) calls the calback,
        // and we do not want to release the transaction
        //finally {
        //    await queryRunner.release();
        //}

        return result;
    }


    /**
     * Experimental one time security context + DID setup.
     * TODO - needs external protection against concurrent races.
     */
    async getAgencyDID(): Promise<DSUDid> {

        let resultResolve;
        let resultReject;
        const result = new Promise<DSUDid>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        //console.log("Wallet returned", wallet);
        resultResolve(this.walletManager.did as GlassDID);

        /*
        getWallet((err, wallet) => {
            if (err)
                return resultReject(err);

            if (!wallet || !wallet.did)
                return resultReject(new Error('Missing data'));

            resultResolve(wallet.did);
        })
        */
        //
        // const id = "COUNTRY_GOV_EID_AGENCY";
        // try {
        //     const sc: DSUSecurityContext = getSCApi().refreshSecurityContext();
        //     sc.on(OpenDSUInitializationEvent, () => {
        //         getDSUDidFactoryRegistry().build(DSUDIDMethods.NAME, "default", id, (err: Err, didDocument: DSUDid) => {
        //             if (err)
        //                 throw err;
        //            resultResolve(didDocument);
        //         });
        //     });
        // } catch (err) {
        //     resultReject(err);
        // }

        return result;
    }


    decodeWalletKeySSI(eGovTaxEntity : EGovTaxEntity): string | undefined {
        if (!eGovTaxEntity || !eGovTaxEntity.walletKeySSI)
            return undefined;

        // if it is already JSON, do not enrich it
        let alreadyJson = false;
        let parsedJson : any = {};

        try {
            parsedJson = JSON.parse(eGovTaxEntity.walletKeySSI);
            alreadyJson = true;
        } catch (e) {
            ;
        }
        if (alreadyJson)
            return parsedJson.payload.keySSI; // TODO not sure this is right
        else
            return eGovTaxEntity.walletKeySSI;
    }

    async parentCreateNewServiceWallet(schema: {}) : Promise<string> {
        const self = this;
        const parentBorestUrl = await self.parentGetRestUrl();
        const postUrl = parentBorestUrl+"/egov/egov/setup/createNewServiceWallet";
        const env: {domain: string, didDomain: string, vaultDomain: string} = getEnvironmentFromProcess();
        const data = { didSchema: schema, domain: env.domain || 'undefined-domain' };
        try {
            info("POST "+postUrl+" "+JSON.stringify(data));
            const parentAxiosReponse = await self.httpService.axiosRef.post(postUrl, data);
            const serviceWalletSeedSSI : string = parentAxiosReponse.data;
            //console.log(serviceWalletSeedSSI);
            info("returned "+serviceWalletSeedSSI+" from POST "+postUrl);
            return serviceWalletSeedSSI;
        } catch (err) {
            if (err && err.response && err.response.data && err.response.data.message) {
                throw new Error(`POST ${postUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                throw new Error(`POST ${postUrl} : Error ${JSON.stringify(err)}`);
            };
        }
    }

    async parentGetRestUrl() : Promise<string> {
        const self = this;
        const parentBorestUrl = await self.arcRepository.findConfigString("egovtax.parent.borestUrl");
        info("parent/borest URL is {0}", parentBorestUrl);
        return parentBorestUrl;
    }
}

