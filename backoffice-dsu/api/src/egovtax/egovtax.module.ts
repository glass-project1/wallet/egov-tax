import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppResourceController } from './appresource.controller';
import { EGovTaxController } from './egovtax.controller';
import { EGovTaxService } from './egovtax.service';
import { AppUserService } from './appuser.service';

@Module({
  imports: [HttpModule,
    TypeOrmModule.forRoot({
    "name": "default",
    "type": "postgres",
    "host": ( process.env.EGOVTAXDB_HOST || "localhost" ),
    "port": ( process.env.EGOVTAXDB_PORT ? parseInt(process.env.EGOVTAXDB_PORT) : 5432 ),
    "username": "egovtax",
    "password": "egovtax",
    "database": "egovtax",
    "entities": [
      "dist/egovtax/*.entity.js"
    ],
    "synchronize": false,
    "logging": true
  })],
  controllers: [
    AppResourceController,
    EGovTaxController,
  ],
  providers: [
    EGovTaxService,
    AppUserService
  ],
  exports: [
    AppUserService
  ],
})
export class EGovTaxModule { }
