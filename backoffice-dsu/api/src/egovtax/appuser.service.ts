import {Injectable} from "@nestjs/common";
import {AppUser} from "./appuser.entity";

@Injectable()
export class AppUserService {
    async findByEmail(email: string): Promise<AppUser[]> {
        console.log("AppUserService.findByEmail AppUser.email=", email);
        return await AppUser.find({where: {email: email}, order: {userId: "DESC"}})
    }
}