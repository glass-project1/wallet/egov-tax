import { Connection } from "typeorm";
import { Controller, Get, Put, Param, Body, Post, UseGuards, Query, NotFoundException } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, getSchemaPath, ApiExtraModels, ApiOkResponse } from "@nestjs/swagger";
import { EGovTaxEntity } from './egovtax.entity';
import { EGovTaxService } from './egovtax.service';
import { EGovTaxQuery, EGovTaxQueryValidator } from "./egovtaxquery.validator";
import { EGovTaxRepository } from "./egovtax.repository";
import { PaginatedDto } from "../paginated.dto";




@ApiExtraModels(PaginatedDto)
@ApiTags('EGovTax')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egovtax/egovtax')
export class EGovTaxController {

    private eGovTaxRepository: EGovTaxRepository;

    constructor(
        private connection: Connection,
        private eGovTaxService: EGovTaxService
    ) {
        this.eGovTaxRepository = this.connection.getCustomRepository(EGovTaxRepository);
    }

    @Get()
    @ApiOperation({summary: "Search for EGovTax based on a query, with paginated results."})
    @ApiOkResponse({
        schema: {
            allOf: [
                { $ref: getSchemaPath(PaginatedDto) },
                {
                    properties: {
                        results: {
                            type: 'array',
                            items: { $ref: getSchemaPath(EGovTaxEntity) },
                        },
                    },
                },
            ],
        },
    })
    async search(@Query(EGovTaxQueryValidator) eGovTaxQuery: EGovTaxQuery): Promise<PaginatedDto<EGovTaxQuery, EGovTaxEntity>> {
        console.log("egovtax.controller.search... query=", eGovTaxQuery);
        const page = await this.eGovTaxRepository.search(eGovTaxQuery);
        console.log("egovtax.controller.search results =", page);
        return page;
    }

    @Get(":id")
    @ApiOperation({ summary: 'Get one EGovTax record' })
    @ApiParam({ name: 'id', type: String, description: "Exact match of primary key column EGovTax.id" })
    @ApiOkResponse({
        type: EGovTaxEntity
    })
    async findOne(@Param() params): Promise<EGovTaxEntity> {
        console.log("egovtax.controller.findOne... id=", params.id);
        let eGovTax = await EGovTaxEntity.findOne(params.id);
        if (!eGovTax) throw new NotFoundException(`Not found EGovTax.id="${params.id}"`);
        console.log("egovtax.controller.findOne egovtax =", eGovTax);
        return eGovTax;
    }

    /* don't allow  update PUT */

    @Post(":id")
    @ApiOperation({ summary: 'Create one wallet for a given ID' })
    @ApiParam({ name: 'id', type: String })
    @ApiOkResponse({
        status: 201,
        type: EGovTaxEntity
    })
    async createWallet(@Param() params, @Body() body: any): Promise<EGovTaxEntity> {
        console.log(`egovtax.controller.post/${params.id}...body`, body);

        const eGovTax = await this.eGovTaxService.createWallet(params.id);
    
        console.log("egovtax.controller.post/${params.id} DB connection closed, egovtax =", eGovTax);
        return eGovTax;
    }


    @Get("/setup/onetime")
    @ApiOperation({ summary: 'Setup a wallet with a DID for this government agency. No need to invoke explicitely, as it will be invoked internally on the 1st use.' })
    @ApiOkResponse({ description: "The returned string is the agency's DID. (Even if invoked more than once, the agency DID is always the same)."})
    async setup() : Promise<string> {
        console.log(`egovtax.controller.setup`);

        const agencyDid = await this.eGovTaxService.getAgencyDID();
    
        console.log("egovtax.controller.setup =", agencyDid);
        return agencyDid.getIdentifier();
    }

}

