
import { HttpException, HttpStatus } from '@nestjs/common';
import { PaginatedDto } from 'src/paginated.dto';
import { createQueryBuilder, EntityRepository, getManager, Repository } from 'typeorm';
import { EGovTaxEntity } from './egovtax.entity';
import { EGovTaxQuery } from './egovtaxquery.validator';

@EntityRepository(EGovTaxEntity)
export class EGovTaxRepository extends Repository<EGovTaxEntity>  {
    constructor(
    ) {
        super();
    }

    /**
     * Performs a SQL query applying the filters according to the @param
     * @param eGovTaxSearchQuery
     */
     async search(eGovTaxSearchQuery: EGovTaxQuery): Promise<PaginatedDto<EGovTaxQuery,EGovTaxEntity>> {
        console.log('egovtax.repository.search query=', eGovTaxSearchQuery)

        const escapeQuote = (str : string): string => {
            if (typeof str === 'string')
                return str.replace(/'/g, "''");
            else
                return str;
        };

        const transformValueToCommaList = (arr: string[] | string): string => {
            arr = Array.isArray(arr) ? arr : [arr]
            return arr.map(value => `'${escapeQuote(value)}'`).join(',');
        }

        const transformValueToEqualOrList = (fieldName: string, arr: string[] | string): string => {
            arr = Array.isArray(arr) ? arr : [arr]
            return "("+arr.map(value => `${fieldName} = '${escapeQuote(value)}'`).join(' OR ')+")";
        }

        const transformValueToLikeList = (fieldName: string, value: string[] | string): string => {
            const values = Array.isArray(value) ? value : [value];
            let str = '';
            let sep = '';
            values.forEach((value: string, index: number) => {
                str += sep;
                str += `${fieldName} ILIKE '%${escapeQuote(value)}%'`;
                sep = ' OR ';
            });
            return "( "+str+" )";
        }

        const getJsonWhereStatement = (fieldName: string, jsonProperty: string, values: string[] | string): string => {
            values = Array.isArray(values) ? values : [values]
            let str = ''
            values.forEach((value: string, index: number) => {
                if (index == 0) {
                    str += `${fieldName} ::jsonb @> \'{"${jsonProperty}":"${escapeQuote(value)}"}\'`
                } else {
                    str += `OR ${fieldName} ::jsonb @> \'{"${jsonProperty}":"${escapeQuote(value)}"}\'`
                }
            })
            return "( "+str+" )";
        }

        const getJsonWhereFieldLikeStatement = (fieldName: string, jsonProperty: string, values: string[] | string): string => {
            values = Array.isArray(values) ? values : [values];
            let str = '';
            let sep = '';
            values.forEach((value: string, index: number) => {
                str += sep;
                str += `${fieldName}::jsonb->>'${jsonProperty}' ILIKE '%${escapeQuote(value)}%'`;
                sep = ' OR ';
            });
            return "( "+str+" )";
        }

        /** NOTE: The name of "whereFunctions" need to be the same name of filter/properties of EventSearchQuery */
        const whereFunctions = {
            id(id: string[] | string): string {
                return `egovtax.id IN (${transformValueToCommaList(id)})`;
            },
            givenName(str: string[]  | string): string {
                return transformValueToLikeList('egovtax.givenname', str);
            },
            surname(str: string[]  | string): string {
                return transformValueToLikeList('egovtax.surname', str);
            }
        }
        const sortProperties = {
            // prop names must match EGovIdQuerySortProperty
            "id":                 "egovtax.id",
            "givenName":          "egovtax.givenName",
            "surname":            "egovtax.surname",
        };

        let queryBuilder = await createQueryBuilder(EGovTaxEntity, 'egovtax');
        //let whereSql : string = '';
        //let whereSqlSep : string = ' AND ';
        for (let [filterName, filterValue] of Object.entries(eGovTaxSearchQuery)) {
            const whereFilter = whereFunctions[filterName]
            if (!!whereFilter) {
                const whereSqlClause = whereFilter(filterValue);
                queryBuilder.andWhere(whereSqlClause);
                //whereSql += `${whereSqlSep}${whereSqlClause}`;
            }
        }
        const orderByProps = Array.isArray(eGovTaxSearchQuery.sortProperty) ? eGovTaxSearchQuery.sortProperty : [eGovTaxSearchQuery.sortProperty];
        const orderByDirs  = Array.isArray(eGovTaxSearchQuery.sortDirection) ? eGovTaxSearchQuery.sortDirection : [eGovTaxSearchQuery.sortDirection];
        if (orderByProps.length != orderByDirs.length) {
            throw new HttpException('sortProperty and sortDirection must have the sane number of values', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        //let sortSql: string = '';
        //let sortSqlSep: string = '';
        let i: number = 0;
        for(i = 0; i<orderByProps.length; i++) {
            const orderByProp = orderByProps[i];
            let sortProp = sortProperties[orderByProp];
            if (!sortProp) {
                throw new HttpException('sortProperty value unsupported. See possible values.', HttpStatus.INTERNAL_SERVER_ERROR);
            }
            const orderByDir = orderByDirs[i];
            queryBuilder.addOrderBy(sortProp, orderByDir);
            //sortSql += `${sortSqlSep}${sortProp} ${orderByDir}`;
            //sortSqlSep = ',';
        }

        console.log(queryBuilder.getSql());
        const count = await queryBuilder.getCount();
        queryBuilder.take(eGovTaxSearchQuery.limit)
        queryBuilder.skip(eGovTaxSearchQuery.page * eGovTaxSearchQuery.limit)
        const egovtaxCollection = await queryBuilder.getMany();

        return {count: count, query: eGovTaxSearchQuery, results: egovtaxCollection };
    }
}
