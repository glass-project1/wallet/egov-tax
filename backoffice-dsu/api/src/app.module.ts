import { Module } from '@nestjs/common';
import { EGovTaxModule } from './egovtax/egovtax.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [EGovTaxModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
