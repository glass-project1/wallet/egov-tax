import { newSpecPage } from '@stencil/core/testing';
import { GlassTaxIncome } from '../glass-tax-income';

describe('glass-tax-income', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassTaxIncome],
      html: `<glass-tax-income></glass-tax-income>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-tax-income>
        <mock:shadow-root>
          <ion-item>
            <ion-icon name="reader-outline" slot="start"></ion-icon>
            <ion-label>
              <h2>
                Tax income
              </h2>
              <p>
                Ana surname
              </p>
            </ion-label>
          </ion-item>
        </mock:shadow-root>
      </glass-tax-income>
    `);
  });
});
