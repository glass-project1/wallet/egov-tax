import { newE2EPage } from '@stencil/core/testing';

describe('glass-tax-income', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-tax-income></glass-tax-income>');

    const element = await page.find('glass-tax-income');
    expect(element).toHaveClass('hydrated');
  });
});
