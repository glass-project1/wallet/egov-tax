# glass-tax-income-gr



<!-- Auto Generated Below -->


## Properties

| Property                      | Attribute          | Description             | Type     | Default            |
| ----------------------------- | ------------------ | ----------------------- | -------- | ------------------ |
| `declaredIncome`              | `declared-income`  |                         | `string` | `"1000€"`          |
| `givenName`                   | `given-name`       | The givenname of the id | `string` | `"Ana"`            |
| `incomeDeclarationFiscalYear` | `declaration-year` |                         | `string` | `"2009"`           |
| `incomeTax`                   | `income-tax`       |                         | `string` | `"dolor sit amet"` |
| `surname`                     | `surname`          | The surname of the id   | `string` | `"dolor sit amet"` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
