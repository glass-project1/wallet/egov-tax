import { newE2EPage } from '@stencil/core/testing';

describe('glass-tax-income-gr', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-tax-income-gr></glass-tax-income-gr>');

    const element = await page.find('glass-tax-income-gr');
    expect(element).toHaveClass('hydrated');
  });
});
