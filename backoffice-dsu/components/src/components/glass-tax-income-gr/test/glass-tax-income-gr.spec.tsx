import { newSpecPage } from '@stencil/core/testing';
import { GlassTaxIncomeGr } from '../glass-tax-income-gr';

describe('glass-tax-income-gr', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassTaxIncomeGr],
      html: `<glass-tax-income-gr></glass-tax-income-gr>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-tax-income-gr>
        <mock:shadow-root>
          <ion-item>
            <ion-icon name="reader-outline" slot="start"></ion-icon>
            <ion-label>
              <h2>
                Tax income
              </h2>
              <p>
                Ana surname
              </p>
            </ion-label>
          </ion-item>
        </mock:shadow-root>
      </glass-tax-income-gr>
    `);
  });
});
