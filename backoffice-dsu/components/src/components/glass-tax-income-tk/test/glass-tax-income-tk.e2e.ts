import { newE2EPage } from '@stencil/core/testing';

describe('glass-tax-income-tk', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-tax-income-tk></glass-tax-income-tk>');

    const element = await page.find('glass-tax-income-tk');
    expect(element).toHaveClass('hydrated');
  });
});
