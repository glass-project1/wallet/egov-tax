import { newSpecPage } from '@stencil/core/testing';
import { GlassTaxIncomeTk } from '../glass-tax-income-tk';

describe('glass-tax-income-tk', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassTaxIncomeTk],
      html: `<glass-tax-income-tk></glass-tax-income-tk>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-tax-income-tk>
        <mock:shadow-root>
          <ion-item>
            <ion-icon name="reader-outline" slot="start"></ion-icon>
            <ion-label>
              <h2>
                Tax income
              </h2>
              <p>
                Ana surname
              </p>
            </ion-label>
          </ion-item>
        </mock:shadow-root>
      </glass-tax-income-tk>
    `);
  });
});
