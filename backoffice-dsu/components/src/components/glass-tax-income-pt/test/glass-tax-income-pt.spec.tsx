import { newSpecPage } from '@stencil/core/testing';
import { GlassTaxIncomePt } from '../glass-tax-income-pt';

describe('glass-tax-income-pt', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassTaxIncomePt],
      html: `<glass-tax-income-pt></glass-tax-income-pt>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-tax-income-pt>
        <mock:shadow-root>
          <ion-item>
            <ion-icon name="reader-outline" slot="start"></ion-icon>
            <ion-label>
              <h2>
                Tax income
              </h2>
              <p>
                Ana surname
              </p>
            </ion-label>
          </ion-item>
        </mock:shadow-root>
      </glass-tax-income-pt>
    `);
  });
});
