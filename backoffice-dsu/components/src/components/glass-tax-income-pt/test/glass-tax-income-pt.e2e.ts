import { newE2EPage } from '@stencil/core/testing';

describe('glass-tax-income-pt', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-tax-income-pt></glass-tax-income-pt>');

    const element = await page.find('glass-tax-income-pt');
    expect(element).toHaveClass('hydrated');
  });
});
