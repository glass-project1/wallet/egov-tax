import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'glass-tax-income-pt',
  styleUrl: 'glass-tax-income-pt.scss',
  shadow: true,
})
export class GlassTaxIncomePt {

  /**
   * The givenname of the id
   */
  @Prop({attribute: "given-name"}) givenName: string = "Ana";

  /**
   * The surname of the id
   */
  @Prop({attribute: "surname"}) surname: string = "dolor sit amet";


  @Prop({attribute: "declaration-year"}) incomeDeclarationFiscalYear: string = "2009";
  @Prop({attribute: "declared-income"}) declaredIncome: string = "1000€";
  @Prop({attribute: "income-tax"}) incomeTax: string = "dolor sit amet";

  render() {
    const self = this;

    return (
      <ion-item>
        <ion-icon name="reader-outline" slot="start"></ion-icon>
        <ion-label>
          <h2>Tax income</h2>
          <p>{self.givenName + " " + "surname"}</p>
        </ion-label>
      </ion-item>
    );
  }

}
