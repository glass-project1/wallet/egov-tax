# sql/install

The scripts under this install folder are supposed to run only once, when the database is setup for the 1st time.

## Files

```sh
setup.sql - create user and databse egovtax (to run as PostgreSQL superuser - typically postgres)
egovtax.sql - create and populate egovtax database (to run as egovtax user)
egovtax_run.sh - to be used from the top level Dockerfile. Do not run manually.
```