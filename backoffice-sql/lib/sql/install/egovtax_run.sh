#!/bin/bash -x
# this is used by ../docker/docker-compose.yml
# it runs (inside the docker only) the egovtax.sql script with the user egovtax
set -e
# remove ON_ERROR_STOP=1 to ignore errors
#psql -v ON_ERROR_STOP=1 --username "egovtax" --dbname "egovtax" -f /docker-entrypoint-initdb.d/60egovtax.norun
# removed because there is an error on a COMMENT, ERROR:  must be owner of extension uuid-ossp
psql -v --username "egovtax" --dbname "egovtax" -f /docker-entrypoint-initdb.d/60egovtax.norun
if [ -n "$GLASS_COUNTRY" ]
then
    if [[ ! "$GLASS_COUNTRY" =~ [A-ZA-Z] ]] ; then echo 2>&1 "env GLASS_COUNTRY must be an ISO 2 letter code upper-case" ; exit 1 ; fi
    GLASS_COUNTRY_LC=$(echo "$GLASS_COUNTRY" | tr '[:upper:]' '[:lower:]')
    psql --echo-all -v --username "egovtax" --dbname "egovtax" <<EOF
DELETE FROM egovtax WHERE country != '${GLASS_COUNTRY}';
UPDATE appresource
    SET value='http://egov-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egovtax.parent.borestUrl';
EOF
else
    echo "WARNING: environment variable GLASS_COUNTRY not set! Preserving default appresource URLs" 2>&1
fi
