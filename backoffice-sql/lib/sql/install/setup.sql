-- run as postgres
create user egovtax password 'egovtax' valid until 'infinity';
create database egovtax owner = egovtax;

-- debug, as we suspect nestjs dies silently
alter system set log_connections = true;
alter system set log_disconnections = true;
select pg_reload_conf();

\connect egovtax

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';

