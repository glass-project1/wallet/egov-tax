#!/bin/bash -xe
# Tool to reset database in egov-id
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov-tax ]
then
    echo 1>&2 "Current working dir must be egov-tax"
    exit 1
fi
cd backoffice-sql
PGPASSWORD=egovtax psql -h localhost egovtax egovtax <<EOF
DROP OWNED BY egovtax;
\i lib/sql/install/egovtax.sql
EOF
