# This docker will have to run with name egov-tax-pt (egov-tax-gr or egov-tax-tk)
# for proper URL configuration on appresource.
#
# ARG GLASS_COUNTRY=2-letter-iso-country-code
#
# Expected port is 8083 for local tests.
#
# See repo egov/Dockerfile for manual building examples.
#
FROM registry.gitlab.com/glass-project1/wallet/node-postgres-docker:latest-dev AS runtime

RUN apk add --no-cache --upgrade --update git

ARG POSTGRES_USER
ARG POSTGRES_PASSWORD

# reasonable defaults - ARG exists for security purposes
ENV POSTGRES_USER=${POSTGRES_USER:-postgres}
ENV POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-postgres}
ENV GLASS_COUNTRY=${GLASS_COUNTRY:-pt}
ENV GLASS_COUNTRY_NAME=${GLASS_COUNTRY_NAME:-Portugal}



# Postgresql comes first, as the nestjs API needs postgresql running.

# The base postgresql-12 image contains a VOLUME directive like
# VOLUME /var/lib/postgresql/data
# and an
# EXPOSE 5432/tcp

# COPY the sql initialization files to the proper path inside the image
COPY backoffice-sql/lib/sql/install/setup.sql /docker-entrypoint-initdb.d/10setup.sql
COPY backoffice-sql/lib/sql/install/egovtax_run.sh /docker-entrypoint-initdb.d/50egovtax_run.sh
COPY backoffice-sql/lib/sql/install/egovtax.sql /docker-entrypoint-initdb.d/60egovtax.norun


# see diff to the .orig for a list of changes to the httpd.conf
# it at least contains the following overrides:
## ProxyPass everything to apihub 8080, except /backoffice and /borest
#ProxyPass        /egov-pt/backoffice !    <-- backoffice frontend
#ProxyPass        /egov-pt/borest http://127.0.0.1:3000/borest    <-- backoffice api
#ProxyPassReverse /egov-pt/borest http://127.0.0.1:3000/borest    <-- backoffice api
#ProxyPass        /egov-pt http://127.0.0.1:8080    <-- OpenDSU Apihub
#ProxyPassReverse /egov-pt http://127.0.0.1:8080    <-- OpenDSU Apihub
#
# destination path for the Apache server is set to /var/www/localhost/htdocs/backoffice

COPY docker/topDocker_etc_apache2_httpd.conf /etc/apache2/httpd.conf


# copy the commands that will be ran by the boot.js script
COPY docker/commands.json /commands.json

ENV WORK_DIR="egovtax"

# create folder egovid
RUN mkdir -p /${WORK_DIR}

COPY backoffice-dsu /${WORK_DIR}/backoffice-dsu
COPY backoffice-dsu /${WORK_DIR}/components
#COPY backoffice-frontend /${WORK_DIR}/backoffice-frontend

#
# backoffice-dsu (includes an api-hub and a nestjs server under api)
#
RUN cd ${WORK_DIR}/backoffice-dsu && \
    npm ci && \
    npm run build-all 

ARG GLASS_ENDPOINT
ENV GLASS_ENDPOINT=${GLASS_ENDPOINT:-"http://localhost:8080"}

ARG EGOV_REST
ENV EGOV_REST=${EGOV_REST:-http://localhost:3000}

ARG ENV_SUFFIX
ENV ENV_SUFFIX=${ENV_SUFFIX}

RUN echo "Environment suffix is ${ENV_SUFFIX}"

ARG ENV_PREFFIX
ENV ENV_PREFFIX=${ENV_PREFFIX}

ARG ADAPTER_ENDPOINT
ENV ADAPTER_ENDPOINT=${ADAPTER_ENDPOINT:-localhost:3000}
RUN echo "Adapter endpoint is ${ADAPTER_ENDPOINT}"

# They will be patched at runtime
RUN cd ${WORK_DIR}/backoffice-dsu && npm run patch-service-config -- %GLASS_ENDPOINT=${GLASS_ENDPOINT}  %EGOV_REST=${EGOV_REST}

# Cleanup
RUN cd ${WORK_DIR} && rm -rf backoffice-dsu/trust-loader-config backoffice-dsu/explorer-patch backoffice-dsu/src backoffice-dsu/api/src
RUN cd ${WORK_DIR}/backoffice-dsu/api && rm -f package_lock.json tsconfig*.json octopus*.json
RUN cd ${WORK_DIR}/backoffice-dsu && rm -f package*.json tsconfig*.json octopus*.json

# Override the Apihub Configs
# RUN cd ${WORK_DIR}/backoffice-dsu && node node_modules/@glass-project1/dsu-utils/src/streamAndReplace.js --srcPath=./overrides/config/ --paramIdentifier='%' --destPath=./apihub-root/external-volume// --baseFolderReplacement="config" %ENV_PREFFIX="$ENV_PREFFIX" 

# RUN cd ${WORK_DIR} && echo $(cat backoffice-dsu/apihub-root/external-volume/config/apihub.json)


##
## backoffice-frontend
##
#RUN cd ${WORK_DIR}/backoffice-frontend && \
#    npm install --unsafe-perm && \
#    npm run build -- --configuration=production && \
#    cp -rp dist/backoffice-frontend /var/www/localhost/htdocs/backoffice

# Cleanup
#RUN rm -rf ${WORK_DIR}/backoffice-dsu/apihub-root/external-volume/config/bdns.hosts
RUN #rm -rf ${WORK_DIR}/backoffice-frontend
RUN apk del git
RUN find /${WORK_DIR} \( -name ".git" -o -name ".gitignore" -o -name ".gitmodules" -o -name ".gitattributes" \) -exec rm -rf -- {} +

# too slow... should be replaced with setting the user and then building the environment. also breaks access to postgres and apache
RUN chown -R node:node /commands.json
RUN chown -R node:node /boot.js
#RUN chown -R node:node /${WORK_DIR}/backoffice-dsu
#
#USER node

RUN cd ${WORK_DIR} && echo $(cat backoffice-dsu/apihub-root/external-volume/config/apihub.json)

EXPOSE 80

# ENV DOMAIN="glass${ENV_SUFFIX}"

CMD ["node", "boot.js"]
